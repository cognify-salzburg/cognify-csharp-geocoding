using Microsoft.EntityFrameworkCore;

namespace GeocodingApi.Models
{
    public class ArbitraryDbContext : DbContext
    {
        
        // The following configures EF to create a Sqlite database file as `C:\blogging.db`.
        // For Mac or Linux, change this to `/tmp/blogging.db` or any other absolute path.
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite(@"Data Source=arbitrary.db");


        public DbSet<ArbitraryDbEntity> ArbitraryDbEntities { get; set; }
    }
}