namespace GeocodingApi.Dto
{
    public class GeocodingResponse {

        public float Latitude {get; set;}

        public float Longitude {get; set;}

        public string Address {get; set;}

        public GeocodingResponse(string address, float lat, float lon) {
            this.Address = address;
            this.Latitude = lat;
            this.Longitude = lon;
        }
    }

}