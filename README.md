# Geocoding
This project contains a skeleton for a RESTful endpoint for resolving addresses to geo coordinates.

## Where to find what?
```
GeocodingApi/Controllers                                 controller
GeocodingApi/Dto                                         predefined DTOs for the API
GeocodingApi/Models                                      database entities and context
GeocodingApi/Program.cs                                  main entrypoint
GeocodingApi/Startup.cs                                  somehow controls some DI stuff
```

## How to make this run?

Depending on your Operating System I cannot tell you in detail. All requirements are specified with the GeocodingApi.csproj

As it's a project using AspNetCore it might be necessary to include some developer certificates on your machine. See `https://docs.microsoft.com/en-us/aspnet/core/security/enforcing-ssl?view=aspnetcore-5.0&tabs=visual-studio#trust-the-aspnet-core-https-development-certificate-on-windows-and-macos` for more detailed information.

With Visual Studio Code and Linux its enough to start using Strg + F5

You should be able to access the api docs url via `http://localhost:5001/swagger/index.html`
A  database file (arbitrary.db) should be created with a test table in it.

No environment variables should be required.

## What is missing?

1. The geocoding endpoint currently returns a dummy response only.  
Please implement a geocoding service that resolves the address tovalid geo coordinates.  
Use Google Maps Geocoding API (https://developers.google.com/maps/documentation/geocoding/overview) to fetch the coordinates.

2. Once the resolution works, please implement a database backed caching of the response.  
For this, create an entity and a repository that allows saving and retrieving of cached geocoding responses.  
A test entity is available in `Models`, feel free to modify/delete/ignore it.

3. As you have more time solving this at home, we appreciate to add unit tests for this little Work as well. You might need no 100% code coverage with your unit tests. We want you to show where you see tests as a tool required to improve
software quality. 

## How to return the result?
Please create a git bundle (https://git-scm.com/book/de/v2/Git-Tools-Bundling) and return the bundle.
