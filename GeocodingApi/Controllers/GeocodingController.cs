﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GeocodingApi.Dto;
using GeocodingApi.Models;


namespace GeocodingApi.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class GeocodingController : ControllerBase
    {

        private readonly ILogger<GeocodingController> _logger;
        private readonly ArbitraryDbContext _dbcontext;

        public GeocodingController(ILogger<GeocodingController> logger, ArbitraryDbContext context = null)
        {
            _logger = logger;
            _dbcontext = context == null ? new ArbitraryDbContext() : context;
        }

        [HttpPost]
        public GeocodingResponse Get(GeocodingRequest request)
        {
            return new GeocodingResponse(request.Adress, 0.0f, 0.0f);
        }
    }
}
